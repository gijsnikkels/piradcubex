﻿#pragma strict

var speed : float = 1.0;
var amount : float = 1.0;
private var py : float;

function Start () {
	py = transform.position.y;
}

function Update () {
	transform.position.y = py + Mathf.Sin(Time.time*speed/(2*Mathf.PI))*amount;
}