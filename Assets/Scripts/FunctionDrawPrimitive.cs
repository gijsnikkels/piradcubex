using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FunctionDrawPrimitive : MonoBehaviour {
	
	public Material diffuse;
	public GameObject[] cubes;
	public int amount = 20;
	[Range(-200,200)]
	public float Translation;
	[Range(-10,10)]
	public float a = 1;
	[Range(-10,10)]
	public float b = 2;
	[Range(-10,10)]
	public float c = 0;



	// Use this for initialization
	void Start() {
		cubes = new GameObject[amount+1];
		for (int i = 0; i <= amount; i++){
			cubes[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
		}
		createFunc();
	}
	
	// Update is called once per frame
	void Update() {
		createFunc();
		GameObject.Find("aVal").GetComponent<Text>().text = ""+Mathf.Round(a);
		GameObject.Find("bVal").GetComponent<Text>().text = ""+b;
		GameObject.Find("cVal").GetComponent<Text>().text = ""+Mathf.Round(c);
	}
	
	void createFunc() {
		float x = -Mathf.PI;
		float dX = (Mathf.PI/(amount/2));


		for (int i = 0; i <= amount; i++){
			float alpha = Mathf.Atan((yPar(x+dX/2)-yPar(x-dX/2))/(dX));

			cubes[i].transform.position = new Vector3(x, yPar(x), 0);
			cubes[i].transform.localScale = new Vector3( dX/Mathf.Cos(alpha), 0.05f, 1f );
			cubes[i].transform.rotation = Quaternion.Euler(0f, 0f, alpha * Mathf.Rad2Deg);
			cubes[i].renderer.sharedMaterial = diffuse;

			x = x + dX;
		}
	}

	float yPar(float iX) {
		return a*Mathf.Pow(iX+Translation, b) + c + 5f;
	}
	
	public void setA(float newA){
		a = newA;
	}
	
	public void setB(float newB){
		b = newB;
	}
	
	public void setC(float newC){
		c = newC;
	}
}