﻿import UnityEngine.UI;

var output : String = "";
var stack : String = "";
var errortext = "Boo.Lang.Compiler.CompilerError:";
function UpdateText () {
	Application.RegisterLogCallback(HandleLog);
	if(output.IndexOf("Boo.Lang.Compiler.CompilerError:")>-1){
		var ind = output.IndexOf(errortext);
		output = output.Replace("iX","x");
		GetComponent(UI.Text).text = output.Substring(ind+errortext.Length);
	}else{
		GetComponent(UI.Text).text = "";
	}
}

function HandleLog (logString : String, stackTrace : String, type : LogType) {
	output = logString;
	stack = stackTrace;
}