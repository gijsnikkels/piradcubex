﻿var FunCube : GameObject;
var ErrorTextObj : GameObject;
var Skin : GUISkin;
//var textfield : GameObject;
private var FuncString : String = "0";
private var height : int = 0;

function setHeight ( HeightN : int ){
	height = HeightN;
}

function getHeight () {
	return height;
}

private var maincam : Camera;
function Start() {
	maincam = Camera.main;
}

private var screenPos : Vector3;
private var LastUpdate:float;
function OnGUI () {
	GUI.skin = Skin;
	screenPos = maincam.WorldToScreenPoint(transform.position);
	FuncString = GUI.TextField (Rect (Screen.width/2-0.3077*Screen.height/2, height+.04615*Screen.height, 0.3077*Screen.height, 0.04153*Screen.height), FuncString, 255);
	var keyPress = false;
	if(Event.current.keyCode == KeyCode.Return)
	{
		if (Time.time >= LastUpdate + .25) {
	        UpdateCubes();
	        LastUpdate = Time.time;
    	}
    }
}

function Update ()
{
    /*if (Time.time >= LastUpdate + .25) {
        UpdateCubes();
        LastUpdate = Time.time;
    }*/
}

function UpdateCubes(){
	ErrorTextObj.GetComponent(ErrorText).UpdateText();
	FunCube.GetComponent(FunctionDrawPrimitive).strSet(FuncString);
	ErrorTextObj.GetComponent(ErrorText).UpdateText();
}