import UnityEngine.UI;
import System.Text.RegularExpressions;

var e = 2.7182818284;
var pi = Mathf.PI;
function pow(n,p){ return Mathf.Pow(n,p); };
function sin(n){ return Mathf.Sin(n); };
function cos(n){ return Mathf.Cos(n); };
function tan(n){ return Mathf.Tan(n); };
function asin(n){ return Mathf.Asin(n); };
function acos(n){ return Mathf.Acos(n); };
function atan(n){ return Mathf.Atan(n); };
function abs(n){ return Mathf.Abs(n); };
function sqrt(n){ return Mathf.Sqrt(n); };
function log(n){ return Mathf.Log(n, 10); };
function logb(n,b){ return Mathf.Log(n, b); };
function ln(n){ return Mathf.Log(n); };

public var userFunc = "0";
function strSet(strF){
	var realRe = "[-]?[0-9x]*\\.?[0-9x]+";
	var realReP = "[]?[0-9x]*\\.?[0-9x]+";
	var insAst = "([0-9ei][xacstple])|(x[0-9acstple])|([0-9xei]\\()|(\\)[0-9xacstple\\(])";
	var insParA = "\\^"+realRe;
	var insParB = realReP+"\\^";
	for(i=1;i<=4;i++){
	var matches = Regex.Matches(strF, insAst);
	if(matches.Count > 0){ // Inserts *
		for each (var match in matches){
			var ind = strF.IndexOf(match.Value);
			strF = strF.Insert(ind+1,"*");
		}
	}}
	matches = Regex.Matches(strF, insParA);
	if(matches.Count > 0){ // Inserts ( and )
		for each (var match in matches){
			ind = strF.IndexOf(match.Value);
			strF = strF.Insert(ind+1,"(");
			strF = strF.Insert(ind+match.length+1,")");
		}
	}
	matches = Regex.Matches(strF, insParB);
	if(matches.Count > 0){ // Inserts ( and )
		for each (var match in matches){
			ind = strF.IndexOf(match.Value);
			strF = strF.Insert(ind,"(");
			strF = strF.Insert(ind+match.length,")");
		}
	}
	while(strF.Contains("^")){
	    var index:int = strF.IndexOf("^");
	    strF = strF.Remove(index,1).Insert(index,",");
	    if (strF.Substring(index+1).StartsWith("(")){ // If the char next to index equals (
	    	strF = strF.Remove(index+1,1); // Just remove the (
	    }else{ 
	    	strF = strF.Insert(index+2,")"); // Don't need this anymore
	    };
	    if (strF.Substring(0,index).EndsWith(")")){
	    	var track = 1;
	    	var powpos = strF.Substring(0,index).LastIndexOf("(");
	    	for(i = index-2; i>=0; i--){
	    		if(strF[i].ToString() == "("){
	    			track--;
	    		}else if(strF[i].ToString() == ")"){
	    			track++;
	    		}
	    		if(track<=0){
	    			powpos = i;
	    			break;
	    		}
	    	}
	    	strF = strF.Remove(index-1,1).Insert(powpos,"pow");
	    }else{
	    	strF = strF.Insert(index-1,"pow(");
	    };
	};
	Debug.Log(strF);
	strF = strF.Replace("x","iX");
	userFunc = strF;
	createFunc();
}

var y:float;
var Yori:float;
function yPar(iX:float):float {
	if (userFunc.Length == 0){userFunc = "0";};
	y = eval(userFunc);
	if (float.IsNaN(y)){ return -65535; }else{ return y+Yori; };
};

var cubes:GameObject[];
var diffuse:Material;
var amount:int = 180;
var xmin = -Mathf.PI;
var xmax = Mathf.PI;
function Start () {
	cubes = new GameObject[amount+1];
	for (i = 0; i <= amount; i++){
		cubes[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
	};
	createFunc();
};

function Update () {
};

function createFunc() {
	var x = xmin;
	var dX = (xmax-xmin)/amount;
	
	for (i = 0; i <= amount; i++){
		var leftY  = yPar(x-dX/2);
		var rightY = yPar(x+dX/2);
		var midY   = (leftY+rightY)/2;
		var alpha  = atan((rightY-leftY)/dX);
		
		if	(leftY == -65535 || midY == -65535 || rightY == -65535 || abs(rightY-leftY)>20){
				cubes[i].renderer.enabled = false;
				midY = -65535;
		}else{
			cubes[i].renderer.enabled = true;
		}
		
		cubes[i].transform.position = Vector3(x, midY, 0);
		cubes[i].transform.localScale = Vector3( dX/cos(alpha), 0.05f, 1f );
		cubes[i].transform.rotation = Quaternion.Euler(0f, 0f, alpha * Mathf.Rad2Deg);
		cubes[i].renderer.sharedMaterial = diffuse;
		
		x = x + dX;
	};
};

