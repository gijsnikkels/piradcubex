﻿#pragma strict

private var pauseGame : boolean = false;
private var showGUI : boolean = false;

function Update()
{
	if(Input.GetKeyDown("escape"))
	{
		pauseGame = !pauseGame;
		
    	if(pauseGame == true)
    	{
    		Time.timeScale = 0;
    		pauseGame = true;
    		showGUI = true;
    	}
    }
    
    if(pauseGame == false)
    {
    	Time.timeScale = 1;
    	pauseGame = false;
    	showGUI = false;
    }
    
    if(showGUI == true)
    {
    	gameObject.Find("PausedGUI").transform.localPosition.y = 0;
    }
    
    else
    {
    	gameObject.Find("PausedGUI").transform.localPosition.y = 30;  
    }
}

function Continue(){
	pauseGame = false;
}