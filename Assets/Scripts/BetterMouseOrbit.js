﻿var target : Transform;
var distance = 10.0;
var distSpeed = 10.0;

var xSpeed = 250.0;
var ySpeed = 120.0;

var yMinLimit = -20;
var yMaxLimit = 80;

private var x = 100;
private var y = 50;

@script AddComponentMenu("Camera-Control/Mouse Orbit")

function Start () {
    var angles = transform.eulerAngles;
    x = angles.y;
    y = angles.x;

	// Make the rigid body not change rotation
   	if (GetComponent.<Rigidbody>())
		GetComponent.<Rigidbody>().freezeRotation = true;
}

function LateUpdate () {
	distance -= Input.GetAxis("Mouse ScrollWheel") * distSpeed;
	distance = Mathf.Clamp(distance,5,500);
    if (target && (Input.GetMouseButton(0) || Input.GetMouseButton(1))) {
        x += Input.GetAxis("Mouse X") * xSpeed * 0.02;
	    y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02;
	        
	    
	    y = ClampAngle(y, yMinLimit, yMaxLimit);
	 		       
    }
    var rotation = Quaternion.Euler(y, x, 0);
	var position = rotation * Vector3(0.0, 0.0, -distance) + target.position + Vector3(0,5.0,0);

	transform.rotation = rotation;
	transform.position = position;
}

static function ClampAngle (angle : float, min : float, max : float) {
	if (angle < -360)
		angle += 360;
	if (angle > 360)
		angle -= 360;
	return Mathf.Clamp (angle, min, max);
}