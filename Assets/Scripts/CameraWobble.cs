using UnityEngine;
using System.Collections;

public class CameraWobble : MonoBehaviour
{
	public float amount = 10.0f;
	public float speed = 1.0f;
	
	void Start () {
	}
	
	void Update () {
		transform.Rotate(Vector3.forward * Mathf.Cos(Time.time*speed)*amount);
	}
}

