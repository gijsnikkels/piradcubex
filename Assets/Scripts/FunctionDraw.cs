﻿/*using UnityEngine;
using System.Collections;

public class FunctionDraw : MonoBehaviour {

	public Terrain FuncDraw;
	[Range(-200,200)]
	public float Translation;
	[Range(-10,10)]
	public float a = 1;
	[Range(-10,10)]
	public float b = 2;
	[Range(-10,10)]
	public float c = 0;

	// Use this for initialization
	void Start() {
		editTerrain();
	}
	
	// Update is called once per frame
	void Update() {
		editTerrain();
	}

	void editTerrain() {
		int xRes = FuncDraw.terrainData.heightmapWidth;
		int zRes = FuncDraw.terrainData.heightmapHeight;
		float dZ = 0;
		float dX = 0;
		float dY = 1f;

		float[,] heights = FuncDraw.terrainData.GetHeights(0,0,xRes,zRes);

		for( int z = 0; z < zRes; z++ ){
			dZ = dZ+(Mathf.PI/100);
			for( int x = 0; x < xRes; x++ ){
				dX = dX+(Mathf.PI/100);
				float zSin = a*Mathf.Sin(dZ+Translation) + a*Mathf.Cos(dX+Translation) + c + dY;
				float zPar = a*Mathf.Pow(dZ+Translation, b) + c + dY;
				heights[x, z] = (zPar)/100;
			}
		}

		FuncDraw.terrainData.SetHeights(0,0,heights);
	}

	public void setA(float newA){
		a = newA;
	}

	public void setB(float newB){
		b = newB;
	}

	public void setC(float newC){
		c = newC;
	}
}*/
